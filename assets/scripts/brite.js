const britecharts = require('britecharts');
const d3 = require('d3-selection');

let datas = document.querySelector('#data-brite').dataset.id;
let datasN1 = document.querySelector('#dataN1-brite').dataset.id;

datas = JSON.parse(datas);
datasN1 = JSON.parse(datasN1);


let dataChartOn = document.querySelector('[data-chart-on]');
if(dataChartOn !== null){

    function createLinesChart() {

        const lineChart1 = new britecharts.line();
        const chartTooltip1 = new britecharts.tooltip();
        //const barChart1 = new britecharts.barContainer();

        // Tooltip options
        chartTooltip1.title('Chiffre d\'affaire');

        // Line chart options
        lineChart1
            .on('customMouseOver', chartTooltip1.show)
            .on('customMouseMove', chartTooltip1.update)
            .on('customMouseOut', chartTooltip1.hide)
            .isAnimated(true)
            .width(1280)
            .colorSchema([
                '#ffce00', '#6aedc7', '#39c2c9', '#ffa71a', '#f866b9', '#998ce3',
                '#edc86a', '#c2c938', '#cb00ff', '#a719ff', '#65b8f7', '#8ce299',
                '#c86aed', '#c938c2', '#00ffcb', '#19ffa7', '#b8f765', '#e2998c',
            ]);

        // barChart1
        //     .width(500)
        //     .height(500);
        
        // barContainer.datum(dataset).call(barChart1);

        // Display line chart
        const chartContainer1 = d3.select('.js-line-chart-1');
        chartContainer1.datum(dataset).call(lineChart1);

        // Display tooltip
        const tooltipContainer1 = chartContainer1.select('.metadata-group .hover-marker'); // Do this only after chart is display, `.hover-marker` is a part of the chart's generated SVG
        tooltipContainer1.datum([]).call(chartTooltip1);

    }

    tabThisYear = [];
    tabLastYear = [];

    for (let i = 1 ; i < 13 ; i++ ) {

        month = i.toString();                
        monthLastYear = i.toString();                
        monthCa = 0;
        monthCaLastYear = 0;
        for (y = 0 ; y < datas.length ; y++) {
            if (i == datas[y].mois) {
                month = datas[y].mois;                
                monthCa = datas[y].monthCa;
            }
        }
        for (x = 0 ; x < datasN1.length ; x++) {
            if (i == datasN1[x].mois) {
                monthLastYear = datasN1[x].mois;                
                monthCaLastYear = datasN1[x].monthCa;
            }
        }
        tabThisYear.push([month, monthCa]);
        tabLastYear.push([monthLastYear, monthCaLastYear]);
    }
    thisYear = new Date().getFullYear();
    lastYear = new Date().getFullYear() - 1;

    const dataset = {
        'dataByTopic': [
            {
                'topic': 0,
                'color': "#6aedc7",
                'dates': [
                    {
                        'date': thisYear + '-01-01T07:00:00.000Z',
                        'value': tabThisYear[0][1],
                        'fullDate': thisYear + '-01-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-02-01T07:00:00.000Z',
                        'value': tabThisYear[1][1],
                        'fullDate': thisYear + '-02-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-03-01T07:00:00.000Z',
                        'value': tabThisYear[2][1],
                        'fullDate': thisYear + '-03-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-04-01T07:00:00.000Z',
                        'value': tabThisYear[3][1],
                        'fullDate': thisYear + '-04-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-05-01T07:00:00.000Z',
                        'value': tabThisYear[4][1],
                        'fullDate': thisYear + '-05-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-06-01T07:00:00.000Z',
                        'value': tabThisYear[5][1],
                        'fullDate': thisYear + '-06-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-07-01T07:00:00.000Z',
                        'value': tabThisYear[6][1],
                        'fullDate': thisYear + '-07-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-08-01T07:00:00.000Z',
                        'value': tabThisYear[7][1],
                        'fullDate': thisYear + '-08-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-09-01T07:00:00.000Z',
                        'value': tabThisYear[8][1],
                        'fullDate': thisYear + '-09-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-10-01T07:00:00.000Z',
                        'value': tabThisYear[9][1],
                        'fullDate': thisYear + '-10-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-11-01T07:00:00.000Z',
                        'value': tabThisYear[10][1],
                        'fullDate': thisYear + '-11-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-12-01T07:00:00.000Z',
                        'value': tabThisYear[11][1],
                        'fullDate': thisYear + '-12-01T07:00:00.000Z'
                    }

                ],
                'topicName': 'n'
            }, {
                'topic': 1,                
                'dates': [
                    {
                        'date': thisYear + '-01-01T07:00:00.000Z',
                        'value': tabLastYear[0][1],
                        'fullDate': lastYear + '-01-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-02-01T07:00:00.000Z',
                        'value': tabLastYear[1][1],
                        'fullDate': lastYear + '-02-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-03-01T07:00:00.000Z',
                        'value': tabLastYear[2][1],
                        'fullDate': lastYear + '-03-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-04-01T07:00:00.000Z',
                        'value': tabLastYear[3][1],
                        'fullDate': lastYear + '-04-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-05-01T07:00:00.000Z',
                        'value': tabLastYear[4][1],
                        'fullDate': lastYear + '-05-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-06-01T07:00:00.000Z',
                        'value': tabLastYear[5][1],
                        'fullDate': lastYear + '-06-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-07-01T07:00:00.000Z',
                        'value': tabLastYear[6][1],
                        'fullDate': lastYear + '-07-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-08-01T07:00:00.000Z',
                        'value': tabLastYear[7][1],
                        'fullDate': lastYear + '-08-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-09-01T07:00:00.000Z',
                        'value': tabLastYear[8][1],
                        'fullDate': lastYear + '-09-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-10-01T07:00:00.000Z',
                        'value': tabLastYear[9][1],
                        'fullDate': lastYear + '-10-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-11-01T07:00:00.000Z',
                        'value': tabLastYear[10][1],
                        'fullDate': lastYear + '-11-01T07:00:00.000Z'
                    }, 
                    {
                        'date': thisYear + '-12-01T07:00:00.000Z',
                        'value': tabLastYear[11][1],
                        'fullDate': lastYear + '-12-01T07:00:00.000Z'
                    }
                ],
                'topicName': 'n -1'
            },
        ]
    };

    createLinesChart();
}
