<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FactureRepository::class)]
class Facture
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'string', length: 255)]
    private $mode_paiement;

    #[ORM\Column(type: 'string', length: 255)]
    private $code_facture;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'factures')]
    private $id_user;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'factures')]
    private $id_customer;

    #[ORM\OneToMany(mappedBy: 'id_facture', targetEntity: ProductFacture::class)]
    private $productFactures;

    public function __construct()
    {
        $this->productFactures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getModePaiement(): ?string
    {
        return $this->mode_paiement;
    }

    public function setModePaiement(string $mode_paiement): self
    {
        $this->mode_paiement = $mode_paiement;

        return $this;
    }

    public function getCodeFacture(): ?string
    {
        return $this->code_facture;
    }

    public function setCodeFacture(string $code_facture): self
    {
        $this->code_facture = $code_facture;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIdCustomer(): ?Customer
    {
        return $this->id_customer;
    }

    public function setIdCustomer(?Customer $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    /**
     * @return Collection<int, ProductFacture>
     */
    public function getProductFactures(): Collection
    {
        return $this->productFactures;
    }

    public function addProductFacture(ProductFacture $productFacture): self
    {
        if (!$this->productFactures->contains($productFacture)) {
            $this->productFactures[] = $productFacture;
            $productFacture->setIdFacture($this);
        }

        return $this;
    }

    public function removeProductFacture(ProductFacture $productFacture): self
    {
        if ($this->productFactures->removeElement($productFacture)) {
            // set the owning side to null (unless already changed)
            if ($productFacture->getIdFacture() === $this) {
                $productFacture->setIdFacture(null);
            }
        }

        return $this;
    }
}
