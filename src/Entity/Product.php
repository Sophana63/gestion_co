<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductRepository::class)]
class Product
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[ORM\Column(type: 'text', nullable: true)]
    private $description;

    #[ORM\Column(type: 'float')]
    private $price_ht;

    #[ORM\Column(type: 'float')]
    private $tva;

    #[ORM\OneToMany(mappedBy: 'id_product', targetEntity: ProductFacture::class)]
    private $productFactures;

    #[ORM\OneToMany(mappedBy: 'id_product', targetEntity: ProductDevis::class)]
    private $productDevis;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'products')]
    private $userId;

    public function __construct()
    {
        $this->productFactures = new ArrayCollection();
        $this->productDevis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPriceHt(): ?float
    {
        return $this->price_ht;
    }

    public function setPriceHt(float $price_ht): self
    {
        $this->price_ht = $price_ht;

        return $this;
    }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(float $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * @return Collection<int, ProductFacture>
     */
    public function getProductFactures(): Collection
    {
        return $this->productFactures;
    }

    public function addProductFacture(ProductFacture $productFacture): self
    {
        if (!$this->productFactures->contains($productFacture)) {
            $this->productFactures[] = $productFacture;
            $productFacture->setIdProduct($this);
        }

        return $this;
    }

    public function removeProductFacture(ProductFacture $productFacture): self
    {
        if ($this->productFactures->removeElement($productFacture)) {
            // set the owning side to null (unless already changed)
            if ($productFacture->getIdProduct() === $this) {
                $productFacture->setIdProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductDevis>
     */
    public function getProductDevis(): Collection
    {
        return $this->productDevis;
    }

    public function addProductDevi(ProductDevis $productDevi): self
    {
        if (!$this->productDevis->contains($productDevi)) {
            $this->productDevis[] = $productDevi;
            $productDevi->setIdProduct($this);
        }

        return $this;
    }

    public function removeProductDevi(ProductDevis $productDevi): self
    {
        if ($this->productDevis->removeElement($productDevi)) {
            // set the owning side to null (unless already changed)
            if ($productDevi->getIdProduct() === $this) {
                $productDevi->setIdProduct(null);
            }
        }

        return $this;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): self
    {
        $this->userId = $userId;

        return $this;
    }
}
