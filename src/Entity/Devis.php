<?php

namespace App\Entity;

use App\Repository\DevisRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DevisRepository::class)]
class Devis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $date;

    #[ORM\Column(type: 'string', length: 255)]
    private $code_devis;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'devis')]
    private $id_user;

    #[ORM\ManyToOne(targetEntity: Customer::class, inversedBy: 'devis')]
    private $id_customer;

    #[ORM\OneToMany(mappedBy: 'id_devis', targetEntity: ProductDevis::class)]
    private $productDevis;

    public function __construct()
    {
        $this->productDevis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCodeDevis(): ?string
    {
        return $this->code_devis;
    }

    public function setCodeDevis(string $code_devis): self
    {
        $this->code_devis = $code_devis;

        return $this;
    }

    public function getIdUser(): ?User
    {
        return $this->id_user;
    }

    public function setIdUser(?User $id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }

    public function getIdCustomer(): ?Customer
    {
        return $this->id_customer;
    }

    public function setIdCustomer(?Customer $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    /**
     * @return Collection<int, ProductDevis>
     */
    public function getProductDevis(): Collection
    {
        return $this->productDevis;
    }

    public function addProductDevi(ProductDevis $productDevi): self
    {
        if (!$this->productDevis->contains($productDevi)) {
            $this->productDevis[] = $productDevi;
            $productDevi->setIdDevis($this);
        }

        return $this;
    }

    public function removeProductDevi(ProductDevis $productDevi): self
    {
        if ($this->productDevis->removeElement($productDevi)) {
            // set the owning side to null (unless already changed)
            if ($productDevi->getIdDevis() === $this) {
                $productDevi->setIdDevis(null);
            }
        }

        return $this;
    }
}
