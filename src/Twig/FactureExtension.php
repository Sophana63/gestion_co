<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FactureExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('round_decimal', [$this, 'roundDecimal']),
            new TwigFilter('saut_de_ligne', [$this, 'sautDeLigne']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('function_name', [$this, 'doSomething']),
        ];
    }

    public function roundDecimal(float $value)
    {
        //return round($value, 2);
        return number_format($value, 2, ',', ' ');
    }

    public function sautDeLigne(string $value)
    {
        //return round($value, 2);
        return nl2br($value);
    }
}
