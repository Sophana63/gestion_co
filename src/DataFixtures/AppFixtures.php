<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\User;
use App\Entity\Devis;
use App\Entity\Facture;
use App\Entity\Product;
use App\Entity\Customer;
use App\Entity\ProductFacture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    
    public function load(ObjectManager $manager): void
    {

        $faker = Faker\Factory::create('fr_FR');
        $this->manager = $manager;
        $user = new User();
            $hashedPassword = $this->passwordHasher->hashPassword($user, 'admin');

            $user->setEmail('admin@admin.fr');
            $user->setPassword($hashedPassword);
            $user->setRoles(['ROLE_ADMIN', 'ROLE_USER']);
            $user->setAdresse($faker->streetAddress);
            $user->setCodePostal(63100);
            $user->setVille($faker->city);
            $user->setTelephone('0473245696');
            $user->setPortable('0618436753');
            $user->setWebsite($faker->domainName);
            $user->setRaisonSociale($faker->company);
            $user->setFormeJuridique('SARL');

            $this->manager->persist($user);

            $users = $this->addUsers();
            $customers = $this->addCustomers($user, $users);
            $products = $this->addProducts($user, $users);
            $factures = $this->addFactures($customers, $user);
            $devis = $this->addDevis($customers, $user);
            $productsFacture = $this->addProductsFacture($factures, $products);

        $this->manager->flush();
    }

    public function addUsers(): array
    {
        $faker = Faker\Factory::create('fr_FR');
        $newUsers = [];

        for ($i = 0; $i < 10; $i++) {
            $newUser = new User();
            $hashedPasswordUser = $this->passwordHasher->hashPassword($newUser, 'user');
            $newUser
                ->setEmail($faker->email)
                ->setPassword($hashedPasswordUser)
                ->setAdresse($faker->streetAddress)
                ->setCodePostal($faker->numberBetween(10000, 95000))
                ->setVille($faker->city)
                ->setTelephone($faker->phoneNumber)
                ->setPortable($faker->phoneNumber)
                ->setWebsite($faker->domainName)
                ->setRaisonSociale($faker->company)
                ->setFormeJuridique($faker->randomElement($array = array ('SARL','Auto-Entreprise','Association', 'SAS')))
                ->setRoles(['ROLE_USER']);
            $this->manager->persist($newUser);
            $newUsers[] = $newUser;
         }

        return $newUsers;
    }

    public function addCustomers($user, array $users): array {

        $faker = Faker\Factory::create('fr_FR');
        $newCustomers = [];

        for ($i = 0; $i < 30; $i++) {
            $newCustomer = new Customer();
            $newCustomer
                ->setLastname($faker->lastName)
                ->setFirstname($faker->lastName)
                ->setAdresse($faker->streetAddress)
                ->setCodePostal($faker->numberBetween(10000, 95000))
                ->setVille($faker->country)
                ->setTelephone($faker->phoneNumber)
                ->setPortable($faker->phoneNumber)
                ->setFax($faker->phoneNumber)
                ->setMail($faker->email)
                ->setWebsite($faker->domainName)
                ->setRaisonSociale($faker->company)
                ->setFormeJuridique($faker->randomElement($array = array ('SARL','Auto-Entreprise','Association', 'SAS', 'Client')))
                ->setUser($faker->randomElement($array = array ($user,$users[array_rand($users, 1)])));
            $this->manager->persist($newCustomer);
            $newCustomers[] = $newCustomer;
        }

        return $newCustomers;
    }

    public function addProducts($user, $users): array {

        $faker = Faker\Factory::create('fr_FR');
        $newProducts = [];

        for ($i = 0; $i < 40; $i++) {
            $newProduct = new Product();
            $newProduct
                ->setName($faker->randomElement($array = array ('Service-' .$i,'Article-' .$i,'Produit-' .$i)))
                ->setDescription($faker->realText(100, 1))
                ->setPriceHt($faker->randomFloat($nbMaxDecimals = 2, $min = 3, $max = 1000))
                ->setTva($faker->randomElement($array = array (20,10,5.5)))
                ->setUserId($faker->randomElement($array = array ($user,$users[array_rand($users, 1)])));
            $this->manager->persist($newProduct);
            $newProducts[] = $newProduct;
        }

        return $newProducts;
    }

    public function addFactures(array $customers, $user): array {

        $faker = Faker\Factory::create('fr_FR');
        $newFactures = [];

        for ($i = 0; $i < 70; $i++) {
            $newFacture = new Facture();
            $newFacture
                //->setDate($faker->dateTimeThisYear($max = 'now', $timezone = null))
                ->setDate($faker->dateTimeBetween($startDate = '2021-01-01', $endDate = '2022-12-30', $timezone = null))
                ->setModePaiement($faker->randomElement($array = array ('Espèce','CB','Chèque', 'SAS', 'Virement')))
                ->setCodeFacture('FA' .date('Y') + time())
                ->setIdCustomer($customers[array_rand($customers, 1)])
                ->setIdUser($user);
            $this->manager->persist($newFacture);
            $newFactures[] = $newFacture;
        }

        return $newFactures;
    }

    public function addDevis(array $customers, $user): array {

        $faker = Faker\Factory::create('fr_FR');
        $newDevis = [];

        for ($i = 0; $i < 11; $i++) {
            $newDev = new Devis();
            $newDev
                ->setDate($faker->dateTimeThisYear($max = 'now', $timezone = null))
                ->setCodeDevis('DE' .date('Y') + time())
                ->setIdCustomer($customers[array_rand($customers, 1)])
                ->setIdUser($user);
            $this->manager->persist($newDev);
            $newDevis[] = $newDev;
        }

        return $newDevis;
    }

    public function addProductsFacture(array $factures, array $products) : array {

        $faker = Faker\Factory::create('fr_FR');
        $newProductsFacture = [];

        for ($i = 0; $i < 150; $i++) {
            $newProductFacture = new ProductFacture();


            $idProduct = $products[array_rand($products, 1)];
            $quantity = $faker->numberBetween($min = 1, $max = 10);
            $totalHT = round($quantity * $idProduct->getPriceHt(), 2);
            $totalTTC = round($totalHT + ($totalHT *  $idProduct->getTva() / 100), 2);
            $newProductFacture
                ->setIdFacture($factures[array_rand($factures, 1)])
                ->setIdProduct($idProduct)
                ->setQuantity($quantity)
                ->setTotalHt($totalHT)
                ->setTotalTtc($totalTTC);
            $this->manager->persist($newProductFacture);
            $newProductsFacture[] = $newProductFacture;  
        }

        return $newProductsFacture;
    }
}
