<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nom' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false,
                    'style' => 'height: 160px',                    
                ]
            ])
            ->add('price_ht', NumberType::class, [
                'label' => 'Prix unitaire HT' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('tva', NumberType::class, [
                'label' => 'TVA' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
