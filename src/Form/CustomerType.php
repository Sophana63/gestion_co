<?php

namespace App\Form;

use App\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options ): void
    {
        $builder
            ->add('lastname', TextType::class, [
                'label' => 'Nom' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('firstname', TextType::class, [
                'label' => 'Prénom' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('adresse', TextType::class, [
                'label' => 'Adresse' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('ville', TextType::class, [
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('code_postal', NumberType::class, [
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('telephone', TextType::class, [
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('portable', TextType::class, [
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('fax', TextType::class, [
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('mail', TextType::class, [
                'label' => 'Email' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('website', TextType::class, [
                'label' => 'Site Web' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('raison_sociale', TextType::class, [
                'label' => 'Raison sociale' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ]
            ])
            ->add('forme_juridique', ChoiceType::class, [
                'label' => 'Forme Juridique' ,
                'attr' => [
                    'class' =>'form-control',
                    'required' => false
                ],
                'choices' => [   
                    '...' =>  ChoiceList::attr($this, function (?Customer $customer) {
                        return  $customer->getFormeJuridique();
                    }),                
                    'SARL' => 'SARL',
                    'SAS' => 'SAS',                    
                    'Association' => 'Association',                    
                    'Auto-Entreprise' => 'Auto-Entreprise',                    
                    'Client' => 'Client',                    
                ],
            ])
            //->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Customer::class,
        ]);
    }
}
