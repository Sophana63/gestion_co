<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class,[
                'constraints' => [
                    new NotBlank([
                        'message' => 'Merci d\'entrer un e-mail',
                    ]),
                ],
                'required' => true,
                'attr' => ['class' =>'input-group'],
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Utilisateur' => 'USER_ROLE',
                    // 'Editeur' => 'ROLE_EDITOR',
                    'Administrateur' => 'ADMIN_ROLE'
                ],
                'expanded' => true,
                'multiple' => true,
                'label' => 'Rôles' ,
                'attr' => ['class' =>'input-group'],
            ])
            ->add('password', PasswordType::class, [
                'label' => 'Mot de passe',
                'attr' => ['class' => 'password-field input-group'],
                'required' => true,
                
            ])
            // ->add('password')
            ->add('adresse', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('ville', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('code_postal', NumberType::class, [
                'attr'  => array(
                    'class' => 'input-group',
                    'required' => false
                    )
            ])
            ->add('telephone', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('portable', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('fax', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('website', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('raison_sociale', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
            ->add('forme_juridique', TextType::class, [
                'attr' => [
                    'class' =>'input-group',
                    'required' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
