<?php

namespace App\Repository;

use App\Entity\ProductFacture;

use Doctrine\ORM\ORMException;
use Doctrine\ORM\EntityManager;
use DoctrineExtensions\Query\Mysql\Year;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ProductFacture|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductFacture|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductFacture[]    findAll()
 * @method ProductFacture[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductFactureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductFacture::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(ProductFacture $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(ProductFacture $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return ProductFacture[] Returns an array of ProductFacture objects
    //  */
    
    public function findCaNow($year)
    {
    
        return $this->createQueryBuilder('p')
            ->addSelect('SUM(p.total_ttc) AS monthCa' )
            ->addSelect('YEAR(f.date) AS annee')
            ->addSelect('MONTH(f.date) AS mois')
            ->join('p.id_facture', 'f')
            ->where('YEAR(f.date) = :year')
            ->setParameter('year', $year)
            ->groupBy('annee')
            ->groupBy('mois')                        
            ->setMaxResults(25)
            ->getQuery()
            ->getResult()
        ;
    }
    

    /*
    public function findOneBySomeField($value): ?ProductFacture
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
