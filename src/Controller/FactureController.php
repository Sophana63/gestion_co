<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductFacture;
use App\Repository\FactureRepository;
use App\Repository\ProductFactureRepository;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FactureController extends AbstractController
{
    #[Route('/facture', name: 'app_facture')]
    public function index(FactureRepository $factureRepository, Request $request, PaginatorInterface $paginator): Response
    {
        $factures = $factureRepository->findBy(['id_user' => $this->getUser()]);
        $facturesByPage = $paginator->paginate(
            $factures, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            10 // Nombre de résultats par page
        );
        return $this->render('facture/index.html.twig', [
            'facturesByPage' => $facturesByPage,
        ]);
    }

    #[Route('/facture/{id}', name: 'app_facture_id')]
    public function factureId(FactureRepository $factureRepository, $id): Response
    {
        $facture = $factureRepository->findOneBy([
            'id' => $id
        ]);
        
        return $this->render('facture/facture.html.twig', [
            'facture' => $facture,
        ]);
    }

    #[Route('/facture/product/del/{id}', name: 'del_facture_product')]
    public function delProduct(EntityManagerInterface $em, ProductFactureRepository $productFactureRepo, $id): Response
    {
        $factureProduct = $productFactureRepo->findOneBy([
            'id' => $id
        ]);

        $em->remove($factureProduct);
        $em->flush();

        $idFacture = $factureProduct->getIdFacture()->getId();

        return $this->redirectToRoute('app_facture_id', [
            'id' => $idFacture,
        ]);
    }

    #[Route('/facture/product/update/{id}/{idProduct}', name: 'update_facture_product')]
    public function updateProduct(Request $request, EntityManagerInterface $em, ProductFactureRepository $productFactureRepo, ProductRepository $productRepo, $id, $idProduct): Response
    {
        $factureProduct = $productFactureRepo->findOneBy([
            'id' => $id,
            'id_product' => $idProduct,
        ]);

        $product = $productRepo->findOneBy([
            'id' => $idProduct,
        ]);

        if (!$factureProduct) {
            throw $this->createNotFoundException(
                'Aucun produit trouvé à l\'id: '.$id. ' et produit: ' .$idProduct,
            );
        }
        if ('POST' === $request->getMethod()) {
            $priceHt = $product->getPriceHt();
            $newPriceHt = $priceHt * $request->get('quantity-name');
            $newPriceTtc = $newPriceHt + ($newPriceHt * $product->getTva() / 100);

            $factureProduct
            ->setQuantity($request->get('quantity-name'))
            ->setTotalHt($newPriceHt)
            ->setTotalTtc($newPriceTtc);

            $product
                ->setDescription($request->get('message-name'))
                ->setPriceHt($request->get('price-name'));

            $em->flush($factureProduct);
            $em->flush($product);
            // dump($request->get('quantity-name'));
            // dump($newPriceHt);
            // dump($newPriceTtc);
            //dump('product=' .$idProduct. ' & productFacture=' .$id)->get_browser; 
        }   

        $idFacture = $factureProduct->getIdFacture()->getId();

        return $this->redirectToRoute('app_facture_id', [
            'id' => $idFacture,
        ]);
    }

    #[Route('/facture/product/new/{id}', name: 'new_facture_product')]
    public function newProduct(Request $request, EntityManagerInterface $em, FactureRepository $factureRepo, ProductRepository $productRepo, $id): Response
    {
        $factureProduct = new ProductFacture();
        $product = new Product();

        $facture = $factureRepo->findOneBy([
            'id' => $id,
        ]);
        
        if ('POST' === $request->getMethod()) {
            $priceHt = $request->get('price-name');
            $newPriceHt = $priceHt * $request->get('quantity-name');
            $newPriceTtc = $newPriceHt + ($newPriceHt * $request->get('tva-name') / 100);

            $product
                ->setName($request->get('ref-name'))
                ->setDescription($request->get('description-name'))
                ->setPriceHt($request->get('price-name'))
                ->setTva($request->get('tva-name'))
                ->setUserId($this->getUser());

            $em->persist($product);
            $em->flush($product);

            $factureProduct
                ->setIdFacture($facture)
                ->setIdProduct($product)
                ->setQuantity($request->get('quantity-name'))
                ->setTotalHt($newPriceHt)
                ->setTotalTtc($newPriceTtc);            

            $em->persist($factureProduct);    
            $em->flush($factureProduct);
        }

        $idFacture = $factureProduct->getIdFacture()->getId();

        return $this->redirectToRoute('app_facture_id', [
            'id' => $idFacture,
        ]);
    }
}
