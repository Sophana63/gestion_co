<?php

namespace App\Controller;

use App\Repository\FactureRepository;
use App\Repository\ProductFactureRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'home')]
    public function index(ProductFactureRepository $productFactureRepository, FactureRepository $factureRepository, Request $request): Response
    {
        $yearNow = date("Y");
        $yearN1 = date("Y") - 1;
        
        return $this->render('home/index.html.twig', [
            'caNow' => $productFactureRepository->findCaNow($yearNow),
            'caN1' => $productFactureRepository->findCaNow($yearN1),
            'nbFactures' => $factureRepository->nbFacturesByYear($yearNow),
            'nbFacturesN1' => $factureRepository->nbFacturesByYear($yearN1),
        ]);
    }
}
