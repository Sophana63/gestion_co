<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220318082513 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE customer (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, lastname VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, adresse VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, code_postal INT DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, portable VARCHAR(255) DEFAULT NULL, fax VARCHAR(255) DEFAULT NULL, mail VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, raison_sociale VARCHAR(255) DEFAULT NULL, forme_juridique VARCHAR(255) DEFAULT NULL, INDEX IDX_81398E09A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devis (id INT AUTO_INCREMENT NOT NULL, id_user_id INT DEFAULT NULL, id_customer_id INT DEFAULT NULL, date DATETIME NOT NULL, code_devis VARCHAR(255) NOT NULL, INDEX IDX_8B27C52B79F37AE5 (id_user_id), INDEX IDX_8B27C52B8B870E04 (id_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE facture (id INT AUTO_INCREMENT NOT NULL, id_user_id INT DEFAULT NULL, id_customer_id INT DEFAULT NULL, date DATETIME NOT NULL, mode_paiement VARCHAR(255) NOT NULL, code_facture VARCHAR(255) NOT NULL, INDEX IDX_FE86641079F37AE5 (id_user_id), INDEX IDX_FE8664108B870E04 (id_customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT DEFAULT NULL, price_ht DOUBLE PRECISION NOT NULL, tva DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_devis (id INT AUTO_INCREMENT NOT NULL, id_devis_id INT DEFAULT NULL, id_product_id INT DEFAULT NULL, quantity INT NOT NULL, total_ht DOUBLE PRECISION NOT NULL, total_ttc DOUBLE PRECISION NOT NULL, INDEX IDX_2A7BBE771105164F (id_devis_id), INDEX IDX_2A7BBE77E00EE68D (id_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_facture (id INT AUTO_INCREMENT NOT NULL, id_facture_id INT DEFAULT NULL, id_product_id INT DEFAULT NULL, quantity INT NOT NULL, total_ht DOUBLE PRECISION NOT NULL, total_ttc DOUBLE PRECISION NOT NULL, INDEX IDX_2F5F7FA0DAA76EDF (id_facture_id), INDEX IDX_2F5F7FA0E00EE68D (id_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, adresse VARCHAR(255) DEFAULT NULL, ville VARCHAR(255) DEFAULT NULL, code_postal INT DEFAULT NULL, telephone VARCHAR(255) DEFAULT NULL, portable VARCHAR(255) DEFAULT NULL, fax VARCHAR(255) DEFAULT NULL, website VARCHAR(255) DEFAULT NULL, raison_sociale VARCHAR(255) DEFAULT NULL, forme_juridique VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B79F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B8B870E04 FOREIGN KEY (id_customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE86641079F37AE5 FOREIGN KEY (id_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE facture ADD CONSTRAINT FK_FE8664108B870E04 FOREIGN KEY (id_customer_id) REFERENCES customer (id)');
        $this->addSql('ALTER TABLE product_devis ADD CONSTRAINT FK_2A7BBE771105164F FOREIGN KEY (id_devis_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE product_devis ADD CONSTRAINT FK_2A7BBE77E00EE68D FOREIGN KEY (id_product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product_facture ADD CONSTRAINT FK_2F5F7FA0DAA76EDF FOREIGN KEY (id_facture_id) REFERENCES facture (id)');
        $this->addSql('ALTER TABLE product_facture ADD CONSTRAINT FK_2F5F7FA0E00EE68D FOREIGN KEY (id_product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B8B870E04');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE8664108B870E04');
        $this->addSql('ALTER TABLE product_devis DROP FOREIGN KEY FK_2A7BBE771105164F');
        $this->addSql('ALTER TABLE product_facture DROP FOREIGN KEY FK_2F5F7FA0DAA76EDF');
        $this->addSql('ALTER TABLE product_devis DROP FOREIGN KEY FK_2A7BBE77E00EE68D');
        $this->addSql('ALTER TABLE product_facture DROP FOREIGN KEY FK_2F5F7FA0E00EE68D');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09A76ED395');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B79F37AE5');
        $this->addSql('ALTER TABLE facture DROP FOREIGN KEY FK_FE86641079F37AE5');
        $this->addSql('DROP TABLE customer');
        $this->addSql('DROP TABLE devis');
        $this->addSql('DROP TABLE facture');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_devis');
        $this->addSql('DROP TABLE product_facture');
        $this->addSql('DROP TABLE user');
    }
}
